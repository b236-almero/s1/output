//A package in Java is used to group related classes. like folders sa File Explorer

//Package naming: reverse name notation
//com is domain, Zuitt is main and example is sub of zuitt
package com.zuitt.example;

public class Variables {
    public static void main (String[] args){
        //Variables-------------------------------
        //Syntax: dataType identifier (use camelCasing)
        int age;
        char middleInitial;

        // variable declaration vs initialization
        int x; //declaration
        int y = 0; //initialization

        x = 1;  //initialization after declaration
        y = 5; //reassignment

        System.out.println("The value of y is "+y+" and the value of x is "+x);

        //Primitive Data Types -------------------------------
        //predefined, used to store "single-valued" variables with limited capabilities.

        //int - whole number values
        int wholeNumber = 100;
        System.out.println(wholeNumber);

        //long - bigger integers,use "L" as suffix to number
        long wordPopulation = 10000000000L;
        System.out.println(wordPopulation);

        //float - integer with decimal values
        float piFloat = 3.141592265359f;
        System.out.println(piFloat);

        //double  - bigger floating point values
        double doubleFloat = 3.141592265359;
        System.out.println(doubleFloat);

        //char - single character
        // uses single quotes
        char letter = 'a';
        System.out.println(letter);

        //boolean - true or false
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        //constants
        //Java adds "final" keyword to create variables value cannot be changed.
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        //Non-Primitive Data Types -------------------------------
        //also know as reference data types that refer to instances or objects.
        //value not stored directly, rather uses the reference to that variable

        //String - sequence of array of characters
        //strings are objects that has methods
        String userName = "JSmith";
        System.out.println(userName);

        //Sample String method
        int stringLength = userName.length();
        System.out.println(stringLength);
    }
}
