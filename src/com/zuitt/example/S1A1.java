package com.zuitt.example;

import java.util.Scanner;

public class S1A1 {
    public static void main(String[] args){
        //STEP 2
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;

        //STEP 3
        Scanner userInput = new Scanner(System.in);

        //STEP 4
        System.out.println("First Name:");
        firstName = userInput.nextLine();

        System.out.println("Last Name:");
        lastName = userInput.nextLine();

        System.out.println("First Subject Grade:");
        firstSubject = userInput.nextDouble();

        System.out.println("Second Subject Grade:");
        secondSubject = userInput.nextDouble();

        System.out.println("Third Subject Grade:");
        thirdSubject = userInput.nextDouble();

        //STEP 5
        double average = (firstSubject + secondSubject + thirdSubject)/3;

        //STEP 6
        System.out.println("Good day, "+ firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + average);


    }
}
