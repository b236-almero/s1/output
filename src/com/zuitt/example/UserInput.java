package com.zuitt.example;

import  java.util.Scanner;

public class UserInput {
    public static void main(String[] args){
        //Object instantiation
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter username: ");

        String userName = myObj.nextLine();
        System.out.println("Username is "+userName);
    }
}
