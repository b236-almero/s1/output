package com.zuitt.example;

import java.util.Scanner;

public class TypeConversion {
    public static void main(String[] args){
        Scanner userInput = new Scanner(System.in);
        System.out.println("How old are you?");

        //convert string input to Double type
        double age = Double.parseDouble(userInput.nextLine());
        System.out.println("This is a confirmation that you are "+age+" years old.");

        System.out.println("How old are you pt2?");
        double age2 = userInput.nextDouble();
        System.out.println("This is a confirmation that you are "+age2+" years old.");

        System.out.println("Enter first number");
        int num1 = userInput.nextInt();

        System.out.println("Enter second number");
        int num2 = userInput.nextInt();

        int sum = num1 + num2;
        System.out.println("Sum is: " + sum);

    }
}
